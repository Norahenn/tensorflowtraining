#!/home/nora/venv/bin/python3
# TensorFlow & tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt

# print(tf.__version__) #To check if every import work

fashion_mnist = keras.datasets.fashion_mnist
# train_images && train_labes arr => training set
# test_images, test_labels arr => test_set
(train_images, train_labels), (test_images, test_lalebs) = fashion_mnist.load_data()

# The labels are an array of integers, from 0 to 9.
# They correspond to the class of clothing the image represents (in this order)
class_names = ['T-shirts/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

# Exploring the format of the dataset (before training the model)

# There's 60 000 images in the training set
# (each images represented as 28x28 pixels)
print(train_images.shape) # => (60000, 28, 28)
# Likewise, there's 60 000 labels in the training set
print(len(train_labels)) # => 60000
# Each label is an integer between 0 and 9
print(train_labels) # => [9 0 0 ... 3 0 5]
# (Though the doc shows : array([9, 0, 0, ..., 3, 0, 5], dtype=uint8))

# 10 000 images of 28*28 px in the test set
print (test_images.shape) # (10000, 28, 28)

# Configure the img to show
plt.figure()
plt.imshow(train_images[0])
plt.colorbar()
plt.grid(False)
# Show the images
plt.show()